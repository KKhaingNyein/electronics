<!-- search box -->
<div class="search-form d-flex align-items-center">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="search-close-btn" id="closeBtn">
                    <i class="fa fa-times-circle"></i>
                </div>
                <form action="#" method="get">
                    <input  type="search" name="caviarSearch" id="search"
                    placeholder="Search Your Desire Items">
                    <input type="submit" class="d-none" value="submit">
                </form>
            </div>
        </div>
    </div>
</div>