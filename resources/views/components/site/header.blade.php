<div class="container-fluid fixed-navbar " id="myHeader" style="background: #fff;">
    <nav class="navbar header navbar-expand-md ">
        <div class="collapse navbar-collapse " id="homeNav">
            <ul class="navbar-nav mr-auto" id="homeMenu">
                <li class="nav-item dropdown ml-2">
                    <a class="nav-link dropdown-toggle text-edited" href="#" id="navbarDropdown" role="button"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                    class="fa fa-th-list" aria-hidden="true"></i></a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <!-- <a class="dropdown-item maindrop" href="listing.html">Categories</a> -->
                        
                        <div class="dropdown-item  dropright subdrop">
                            <span>
                                Computers & Accessories
                            </span>
                            
                            <i class="fa fa-angle-right ml-2"></i>
                            <div class="dropdown-menu">
                                <div class="row">
                                    <div class="col-md-4 mb-3">
                                        <h3>Computers & Accessories</h3>
                                        <hr>
                                        <h6><a href="" class="text-dark" >All Computers & Accessories</a></h6>
                                        <h6><a href="" class="text-dark" >Laptops, Desktops & Monitors</a></h6>
                                        <h6><a href="" class="text-dark" >Networking & Internet Devices</a></h6>
                                        <h6><a href="" class="text-dark" >Computer Accessories</a></h6>
                                        <h6><a href="" class="text-dark" >Software</a></h6>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <h3>Accessories & Parts</h3>
                                        <hr>
                                        <h6><a href="" class="text-dark" >Stickers & Skins</a></h6>
                                        <h6><a href="" class="text-dark" >Memory Card</a></h6>
                                        <h6><a href="" class="text-dark" >Adapters</a></h6>
                                        <h6><a href="" class="text-dark" >Card Reader</a></h6>
                                        <h6><a href="" class="text-dark" >Screen Reader</a></h6>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <h3>Smart Electronics</h3>
                                        <hr>
                                        <h6><a href="" class="text-dark" >Bluetooth</a></h6>
                                        <h6><a href="" class="text-dark" >Touch Screen</a></h6>
                                        <h6><a href="" class="text-dark" >GPS Navigation</a></h6>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <h3>Computer Parts</h3>
                                        <hr>
                                        <h6><a href="" class="text-dark" >RAM</a></h6>
                                        <h6><a href="" class="text-dark" >CPU</a></h6>
                                        <h6><a href="" class="text-dark" >Hark Disk</a></h6>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <h3>Office & Stationery</h3>
                                        <hr>
                                        <h6><a href="" class="text-dark" >All Office & Stationery</a></h6>
                                        <h6><a href="" class="text-dark" >Printers & Ink</a></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="dropdown-item  dropright subdrop">
                            <span>
                                Mobiles & Tablets
                            </span>
                            
                            <i class="fa fa-angle-right ml-2"></i>
                            <div class="dropdown-menu">
                                <div class="row">
                                    <div class="col-md-4 mb-3">
                                        <h3>Mobiles & Tablets</h3>
                                        <hr>
                                        <h6><a href="" class="text-dark" >All Mobile Phones</a></h6>
                                        <h6><a href="" class="text-dark" >SmartPhones</a></h6>
                                        <h6><a href="" class="text-dark" >Refurbished Mobiles</a></h6>
                                    </div>
                                    
                                    <div class="col-md-4 mb-3">
                                        <h3>All Mobile Accessories</h3>
                                        <hr>
                                        <h6><a href="" class="text-dark" >Adapters</a></h6>
                                        <h6><a href="" class="text-dark" >Power Bank</a></h6>
                                        <h6><a href="" class="text-dark" >Screen Guard</a></h6>
                                        <h6><a href="" class="text-dark" >Cover</a></h6>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <h3>All Tablet Accessories</h3>
                                        <hr>
                                        <h6><a href="" class="text-dark" >Adapters</a></h6>
                                        <h6><a href="" class="text-dark" >Power Bank</a></h6>
                                        <h6><a href="" class="text-dark" >Screen Guard</a></h6>
                                        <h6><a href="" class="text-dark" >Cover</a></h6>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <h3>All Electronics</h3>
                                        <hr>
                                        <h6><a href="" class="text-dark" >Cases & Covers</a></h6>
                                        <h6><a href="" class="text-dark" >Discover more Products</a></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="dropdown-item  dropright subdrop">
                            <span>
                                Cameras, Audio & Video
                            </span>
                            
                            <i class="fa fa-angle-right ml-2"></i>
                            <div class="dropdown-menu">
                                <div class="row">
                                    <div class="col-md-4 mb-3">
                                        <h3>All Cameras</h3>
                                        <hr>
                                        <h6><a href="" class="text-dark" >Flying Camera</a></h6>
                                        <h6><a href="" class="text-dark" >Gopro Camera</a></h6>
                                        <h6><a href="" class="text-dark" >Multifunction Camera</a></h6>
                                        <h6><a href="" class="text-dark" >Multifunction Camera Stand</a></h6>
                                        <h6><a href="" class="text-dark" >Olloclip Camera</a></h6>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <h3>Cameras & Accessories</h3>
                                        <hr>
                                        <h6><a href="" class="text-dark" >All Cameras Accessories</a></h6>
                                        <h6><a href="" class="text-dark" >Camera Stand</a></h6>
                                        <h6><a href="" class="text-dark" >Camera Print</a></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="dropdown-item  dropright subdrop">
                            <span>
                                Movie & Video Games
                            </span>
                            
                            <i class="fa fa-angle-right ml-2"></i>
                            <div class="dropdown-menu">
                                <div class="row">
                                    <div class="col-md-4 mb-3">
                                        <h3>Movies & TV Show</h3>
                                        <hr>
                                        <h6><a href="" class="text-dark" >All Movies & TV Shows</a></h6>
                                        <h6><a href="" class="text-dark" >All English</a></h6>
                                        <h6><a href="" class="text-dark" >All Hindi</a></h6>
                                        <h6><a href="" class="text-dark" >All KPop</a></h6>
                                        <h6><a href="" class="text-dark" >All JPop</a></h6>
                                    </div>
                                    
                                    <div class="col-md-4 mb-3">
                                        <h3>Music</h3>
                                        <hr>
                                        <h6><a href="" class="text-dark" >All Music</a></h6>
                                        <h6><a href="" class="text-dark" >Indian Classical</a></h6>
                                        <h6><a href="" class="text-dark" >Musical Instruments</a></h6>
                                        <h6><a href="" class="text-dark" >English</a></h6>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <h3>Video Games</h3>
                                        <hr>
                                        <h6><a href="" class="text-dark" >PC Games</a></h6>
                                        <h6><a href="" class="text-dark" >Consoles</a></h6>
                                        <h6><a href="" class="text-dark" >Accessories</a></h6>
                                        <h6><a href="" class="text-dark" >Game Sets</a></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="dropdown-item  dropright subdrop">
                            <span>
                                TV & Audio
                            </span>
                            
                            <i class="fa fa-angle-right ml-2"></i>
                            <div class="dropdown-menu">
                                <div class="row">
                                    <div class="col-md-4 mb-3">
                                        <h3>Audio & Video</h3>
                                        <hr>
                                        <h6><a href="" class="text-dark" >All Audio & Video</a></h6>
                                        <h6><a href="" class="text-dark" >Televisions</a></h6>
                                        <h6><a href="" class="text-dark" >Headphones</a></h6>
                                        <h6><a href="" class="text-dark" >Speakers</a></h6>
                                        <h6><a href="" class="text-dark" >Audio & Video Accessories</a></h6>
                                    </div>
                                    
                                    <div class="col-md-4 mb-3">
                                        <h3>Music</h3>
                                        <hr>
                                        <h6><a href="" class="text-dark" >Television</a></h6>
                                        <h6><a href="" class="text-dark" >HeadPhones</a></h6>
                                        <h6><a href="" class="text-dark" >Earphones</a></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="dropdown-item  dropright subdrop">
                            <span>
                                Watches
                            </span>
                            
                            <i class="fa fa-angle-right ml-2"></i>
                            <div class="dropdown-menu">
                                <div class="row">
                                    <div class="col-md-4 mb-3">
                                        <h3>Watches</h3>
                                        <hr>
                                        <h6><a href="" class="text-dark" >All Watches</a></h6>
                                        <h6><a href="" class="text-dark" >Men's Watches</a></h6>
                                        <h6><a href="" class="text-dark" >Women's Watches</a></h6>
                                        <h6><a href="" class="text-dark" >Premium Watches</a></h6>
                                        <h6><a href="" class="text-dark" >Deals on Watches</a></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <!-- MMenu.js-->
        <!-- <a href="#my-menu" class="nav-link text-edited mmenu"><i class="fa fa-th-list" aria-hidden="true"></i></a>
        <nav id="my-menu">
            <ul>                              
                <li><span>Cameras, Audio & Video</span>
                    <ul>
                        <li>
                            <span>Computers & Accessories</span>
                            <ul>
                                <li>
                                    <a href="">All Computers & Accessories</a>
                                </li>
                                <li>
                                    <a href="">Laptops, Desktops & Monitors</a>                                                                    
                                </li>
                                <li>
                                    <a href="">Networking & Internet Devices</a>
                                </li>
                                <li>
                                    <a href="">Computer Accessories</a>
                                </li>
                                <li>
                                    <a href="">Software</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>Accessories & Parts</span>
                            <ul>
                                <li>
                                    <a href="">Stickers & Skins</a>
                                </li>
                                <li>
                                    <a href="">Memory Card</a>
                                </li>
                                <li>
                                    <a href="">Adapters</a>
                                </li>
                                <li>
                                    <a href="">Card Reader</a>
                                </li>
                                <li>
                                    <a href="">Screen Reader</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>Smart Electronics</span>
                            <ul>
                                <li>
                                    <a href="">Bluetooth</a>
                                </li>
                                <li>
                                    <a href="">Touch Screen</a>
                                </li>
                                <li>
                                    <a href="">GPS Navigation</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>Computer Parts</span>
                            <ul>
                                <li>
                                    <a href="">RAM</a>
                                </li>
                                <li>
                                    <a href="">CPU</a>
                                </li>
                                <li>
                                    <a href="">Hark Disk</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>Office & Stationery</span>
                            <ul>
                                <li>
                                    <a href="">All Office & Stationery</a>
                                </li>
                                <li>
                                    <a href="">Printers & Ink</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><span>Mobiles & Tablets</span>
                    <ul>
                        <li>
                            <span>Mobiles & Tablets</span>
                            <ul>
                                <li>
                                    <a href="">All Mobile Phones</a>
                                </li>
                                <li>
                                    <a href="">Refurbished Mobiles</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>All Mobile Accessories</span>
                            <ul>
                                <li>
                                    <a href="">Adapters</a>
                                </li>
                                <li>
                                    <a href="">Power Bank</a>
                                </li>
                                <li>
                                    <a href="">Screen Guard</a>
                                </li>
                                <li>
                                    <a href="">Cover</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>All Tablet Accessories</span>
                            <ul>
                                <li>
                                    <a href="">Adapters</a>
                                </li>
                                <li>
                                    <a href="">Power Bank</a>
                                </li>
                                <li>
                                    <a href="">Screen Guard</a>
                                </li>
                                <li>
                                    <a href="">Cover</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>All Electronics</span>
                            <ul>
                                <li>
                                    <a href="">Cases & Covers</a>
                                </li>
                                <li>
                                    <a href="">Discover more Products</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li> 
                <li><span>Computers & Accessories</span>
                    <ul>
                        <li>
                            <span>All Cameras</span>
                            <ul>
                                <li>
                                    <a href="">Flying Camera</a>
                                </li>
                                <li>
                                    <a href="">Gopro Camera</a>                                                                    
                                </li>
                                <li>
                                    <a href="">Multifunction Camera</a>
                                </li>
                                <li>
                                    <a href="">Multifunction Camera Stand</a>
                                </li>
                                <li>
                                    <a href="">Olloclip Camera</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>Cameras & Accessories</span>
                            <ul>
                                <li>
                                    <a href="">All Cameras Accessories</a>
                                </li>
                                <li>
                                    <a href="">Camera Stand</a>
                                </li>
                                <li>
                                    <a href="">Camera Print</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><span>Movie & Video Games</span>
                    <ul>
                        <li>
                            <span>Computers & Accessories</span>
                            <ul>
                                <li>
                                    <a href="">All Computers & Accessories</a>
                                </li>
                                <li>
                                    <a href="">Laptops, Desktops & Monitors</a>                                                                    
                                </li>
                                <li>
                                    <a href="">Networking & Internet Devices</a>
                                </li>
                                <li>
                                    <a href="">Computer Accessories</a>
                                </li>
                                <li>
                                    <a href="">Software</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>Accessories & Parts</span>
                            <ul>
                                <li>
                                    <a href="">Stickers & Skins</a>
                                </li>
                                <li>
                                    <a href="">Memory Card</a>
                                </li>
                                <li>
                                    <a href="">Adapters</a>
                                </li>
                                <li>
                                    <a href="">Card Reader</a>
                                </li>
                                <li>
                                    <a href="">Screen Reader</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>Smart Electronics</span>
                            <ul>
                                <li>
                                    <a href="">Bluetooth</a>
                                </li>
                                <li>
                                    <a href="">Touch Screen</a>
                                </li>
                                <li>
                                    <a href="">GPS Navigation</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>Computer Parts</span>
                            <ul>
                                <li>
                                    <a href="">RAM</a>
                                </li>
                                <li>
                                    <a href="">CPU</a>
                                </li>
                                <li>
                                    <a href="">Hark Disk</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>Office & Stationery</span>
                            <ul>
                                <li>
                                    <a href="">All Office & Stationery</a>
                                </li>
                                <li>
                                    <a href="">Printers & Ink</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><span>TV & Audio</span>
                    <ul>
                        <li>
                            <span>Computers & Accessories</span>
                            <ul>
                                <li>
                                    <a href="">All Computers & Accessories</a>
                                </li>
                                <li>
                                    <a href="">Laptops, Desktops & Monitors</a>                                                                    
                                </li>
                                <li>
                                    <a href="">Networking & Internet Devices</a>
                                </li>
                                <li>
                                    <a href="">Computer Accessories</a>
                                </li>
                                <li>
                                    <a href="">Software</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>Accessories & Parts</span>
                            <ul>
                                <li>
                                    <a href="">Stickers & Skins</a>
                                </li>
                                <li>
                                    <a href="">Memory Card</a>
                                </li>
                                <li>
                                    <a href="">Adapters</a>
                                </li>
                                <li>
                                    <a href="">Card Reader</a>
                                </li>
                                <li>
                                    <a href="">Screen Reader</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>Smart Electronics</span>
                            <ul>
                                <li>
                                    <a href="">Bluetooth</a>
                                </li>
                                <li>
                                    <a href="">Touch Screen</a>
                                </li>
                                <li>
                                    <a href="">GPS Navigation</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>Computer Parts</span>
                            <ul>
                                <li>
                                    <a href="">RAM</a>
                                </li>
                                <li>
                                    <a href="">CPU</a>
                                </li>
                                <li>
                                    <a href="">Hark Disk</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>Office & Stationery</span>
                            <ul>
                                <li>
                                    <a href="">All Office & Stationery</a>
                                </li>
                                <li>
                                    <a href="">Printers & Ink</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><span>Watches</span>
                    <ul>
                        <li>
                            <span>Computers & Accessories</span>
                            <ul>
                                <li>
                                    <a href="">All Computers & Accessories</a>
                                </li>
                                <li>
                                    <a href="">Laptops, Desktops & Monitors</a>                                                                    
                                </li>
                                <li>
                                    <a href="">Networking & Internet Devices</a>
                                </li>
                                <li>
                                    <a href="">Computer Accessories</a>
                                </li>
                                <li>
                                    <a href="">Software</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>Accessories & Parts</span>
                            <ul>
                                <li>
                                    <a href="">Stickers & Skins</a>
                                </li>
                                <li>
                                    <a href="">Memory Card</a>
                                </li>
                                <li>
                                    <a href="">Adapters</a>
                                </li>
                                <li>
                                    <a href="">Card Reader</a>
                                </li>
                                <li>
                                    <a href="">Screen Reader</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>Smart Electronics</span>
                            <ul>
                                <li>
                                    <a href="">Bluetooth</a>
                                </li>
                                <li>
                                    <a href="">Touch Screen</a>
                                </li>
                                <li>
                                    <a href="">GPS Navigation</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>Computer Parts</span>
                            <ul>
                                <li>
                                    <a href="">RAM</a>
                                </li>
                                <li>
                                    <a href="">CPU</a>
                                </li>
                                <li>
                                    <a href="">Hark Disk</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>Office & Stationery</span>
                            <ul>
                                <li>
                                    <a href="">All Office & Stationery</a>
                                </li>
                                <li>
                                    <a href="">Printers & Ink</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav> -->
        
        <img src="{{asset('images/small_logo.png')}}" class="d-none d-sm-block" alt="logo" style="margin-right: auto; margin-left: auto;">
        <img src="{{asset('images/small_logo.png')}}" class=" d-block d-sm-none" alt="logo" style="margin-right: auto; margin-left: auto; max-width: 150px;">
        <div class="search-btn pr-4">
            <a id="search-btn" href="#" class="text-edited"><i class="fa fa-search mt-1" aria-hidden="true"></i></a>
        </div>
        <div class="pr-3 cart-btn">
            <a href="#" class="text-edited">
                <i class="fa fa-shopping-bag"></i>
            </a>
        </div>
        <div class="pr-3 user-btn">
            <a href="#" class="text-edited">
                <i class="fa fa-user"></i> 
            </a>
        </div>
        <div class="help-btn">
            <a href="#" class="text-edited">
                <i class="fa fa-question-circle"></i>
            </a>
        </div>
    </nav>
</div>