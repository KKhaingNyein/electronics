@extends('layouts.app')

@section('title', 'Electronics')

@section('meta')
@endsection

@section('classes', 'homepage home-page front-page')

@section('content')

@include('site.home.slider')
@include('site.home.ads')
@include('site.home.new-arrival')
@include('site.home.discount')
@include('site.home.interval')
@include('site.home.computer')

@endsection