<section class="discount mt-5">
    <div class="container">
        <div class="d-flex flex-row flex-wrap">
            <div class="new-arrival-header mx-auto">
                <hr class="m-0 w-75 mx-auto header_hr">
                <h3 class="font-weight-bold text-edited">Discount</h3>
            </div>
        </div>
        <div class="d-flex flex-wrap flex-row mb-1">
            <small class="text-edited ml-auto">
                more
                <i class="fas fa-angle-double-right"></i>
            </small>
        </div>
        <!-- Swiper -->
        <div class="swiper-container swiperItem">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="card card-product border h-100">
                        <a href="#">
                            <img class="card-img-top " style="height: 150px;" src="{{asset('images/items/acmwv82lla.jpg')}}" alt="Card image cap">
                            <div class="card-body">
                                <h6 class="card-title mb-0">iMac Pro (4TB SSD,256GB RAM)</h6>
                                <p class="small p-0 m-0 text-dark"><s>$400.56</s></p>
                                <p class="card-text h6">$399,99</p>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex flex-wrap flex-row">
                                    <div class="w-50">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                    <div class="w-50 text-right">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </a>
                        <div class="overlay">
                            <div class="detail-btn">
                                <a href="#" class="btn btn-sm btn-light font-weight-bold">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="card card-product border h-100">
                        <a href="#">
                            <img class="card-img-top " style="height: 150px;" src="{{asset('images/items/9421510410270.jpg')}}" alt="Card image cap">
                            <div class="card-body">
                                <h6 class="card-title mb-0">iPad WiFi, 10.2", 32 GB, Gold (2019)</h6>
                                <p class="small p-0 m-0 text-dark"><s>$345.56</s></p>
                                <p class="card-text h6">$344</p>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex flex-wrap flex-row">
                                    <div class="w-50">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                    <div class="w-50 text-right">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </a>
                        <div class="overlay">
                            <div class="detail-btn">
                                <a href="#" class="btn btn-sm btn-light font-weight-bold">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="card card-product border h-100">
                        <a href="#">
                            <img class="card-img-top " style="height: 150px;" src="{{asset('images/items/9409065517086.jpg')}}" alt="Card image cap">
                            <div class="card-body">
                                <h6 class="card-title mb-0">NINTENDO Switch New Neon 32 GB</h6>
                                <p class="small p-0 m-0 text-dark"><s>$301.56</s></p>
                                <p class="card-text h6">$299</p>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex flex-wrap flex-row">
                                    <div class="w-50">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                    <div class="w-50 text-right">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </a>
                        <div class="overlay">
                            <div class="detail-btn">
                                <a href="#" class="btn btn-sm btn-light font-weight-bold">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="card card-product border h-100">
                        <a href="#">
                            <img class="card-img-top " style="height: 150px;" src="{{asset('images/items/JBL_E55BT_KEY_WHITE_6165_FS_x1-1605x1605px.jpg')}}" alt="Card image cap">
                            <div class="card-body">
                                <h6 class="card-title mb-0">JBL E55BT Wireless over-ear</h6>
                                <p class="small p-0 m-0 text-dark"><s>$7999</s></p>
                                <p class="card-text h6">$6999</p>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex flex-wrap flex-row">
                                    <div class="w-50">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                    <div class="w-50 text-right">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </a>
                        <div class="overlay">
                            <div class="detail-btn">
                                <a href="#" class="btn btn-sm btn-light font-weight-bold">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="card card-product border h-100">
                        <a href="#">
                            <img class="card-img-top " style="height: 150px;" src="{{asset('images/items/9361027334174.jpg')}}" alt="Card image cap">
                            <div class="card-body">
                                <h6 class="card-title mb-0">TOM TOM TOM VIA 53 EU gameplay</h6>
                                <p class="small p-0 m-0 text-dark"><s>$210.56</s></p>
                                <p class="card-text h6">$109</p>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex flex-wrap flex-row">
                                    <div class="w-50">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                    <div class="w-50 text-right">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </a>
                        <div class="overlay">
                            <div class="detail-btn">
                                <a href="#" class="btn btn-sm btn-light font-weight-bold">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>