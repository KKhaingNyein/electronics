<section class="interval my-5">
    <div class="container_fluid interval">
        <div class="container">
            <div class="d-flex flex-wrap flex-row">
                <div class="w-25 left-interval">
                    <!-- <img src="src/img/interval/left_interval5.jpg" alt="" class="w-100 h-100 float-right"> -->
                </div>
                <div class="w-50 pt-2 text-white text-center">
                    <h3>Life is easier on iPhone.</h3>
                    <h6>And that starts as soon as you turn it on.</h6>
                </div>
                <div class="w-25 right-interval">
                    <!-- <img src="src/img/interval/right_interval.jpg" alt="" class="w-100 h-100"> -->
                </div>
            </div>
        </div>
    </div>
</section>