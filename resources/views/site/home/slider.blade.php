<section class="homeimg-slider">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100 h-100 border-top" src="{{asset('images/sliderimg/iphone11s.jpg')}}" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100 h-100 border-top" src="{{asset('images/sliderimg/watch5ava.jpg')}}" alt="Third slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100 h-100 border-top" src="{{asset('images/sliderimg/galaxynot10.jpg')}}" alt="Third slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100 h-100 border-top" src="{{asset('images/sliderimg/ipad10.jpg')}}" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>