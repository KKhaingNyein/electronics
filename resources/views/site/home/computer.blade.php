<section class="computer mt-5">
    <div class="container">
        <div class="d-flex flex-row flex-wrap">
            <div class="new-arrival-header mx-auto">
                <hr class="m-0 w-75 mx-auto header_hr">
                <h3 class="font-weight-bold text-edited">Computers & Accessories</h3>
            </div>
        </div>
        <div class="d-flex flex-wrap flex-row mb-1">
            <small class="text-edited ml-auto">
                more
                <i class="fas fa-angle-double-right"></i>
            </small>
        </div>
        <!-- Swiper -->
        <div class="swiper-container swiperItem">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="card card-product border h-100">
                        <a href="#">
                            <img class="card-img-top " style="height: 150px;" src="{{asset('images/items/computers/Untitled-1.jpg')}}" alt="Card image cap">
                            <div class="card-body">
                                <h6 class="card-title mb-0">Apple MacBook Air Gold (Previous Model)</h6>
                                <p class="card-text h6">$1,149</p>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex flex-wrap flex-row">
                                    <div class="w-50">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                    <div class="w-50 text-right">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </a>
                        <div class="overlay">
                            <div class="detail-btn">
                                <a href="#" class="btn btn-sm btn-light font-weight-bold">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="card card-product border h-100">
                        <a href="#">
                            <img class="card-img-top " style="height: 150px;" src="{{asset('images/items/computers/Great-ShopUSA-Latops-Offers.jpg')}}" alt="Card image cap">
                            <div class="card-body">
                                <h6 class="card-title mb-0">ASUS ZenBook UX533 Ultra Slim Compact Laptop</h6>
                                <p class="card-text h6">$1,299</p>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex flex-wrap flex-row">
                                    <div class="w-50">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                    <div class="w-50 text-right">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </a>
                        <div class="overlay">
                            <div class="detail-btn">
                                <a href="#" class="btn btn-sm btn-light font-weight-bold">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="card card-product border h-100">
                        <a href="#">
                            <img class="card-img-top " style="height: 150px;" src="{{asset('images/items/computers/Untitled-2.jpg')}}" alt="Card image cap">
                            <div class="card-body">
                                <h6 class="card-title mb-0">Dell Inspiron 7000 Series Touchscreen flexiable</h6>
                                <p class="card-text h6">$1,099</p>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex flex-wrap flex-row">
                                    <div class="w-50">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                    <div class="w-50 text-right">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </a>
                        <div class="overlay">
                            <div class="detail-btn">
                                <a href="#" class="btn btn-sm btn-light font-weight-bold">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="card card-product border h-100">
                        <a href="#">
                            <img class="card-img-top " style="height: 150px;" src="{{asset('images/items/computers/b3973266-856e-424b-89c5.jpg')}}" alt="Card image cap">
                            <div class="card-body">
                                <h6 class="card-title mb-0">Acer SB220Q bi 21.5 inches Full HD Ultra-Thin nonFrame</h6>
                                <p class="card-text h6">$89.99</p>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex flex-wrap flex-row">
                                    <div class="w-50">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                    <div class="w-50 text-right">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </a>
                        <div class="overlay">
                            <div class="detail-btn">
                                <a href="#" class="btn btn-sm btn-light font-weight-bold">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="card card-product border h-100">
                        <a href="#">
                            <img class="card-img-top " style="height: 150px;" src="{{asset('images/items/computers/cdf1bef0-2dc2-467f-bebe-e1247a1e1865._SR300,300_.jpg')}}" alt="Card image cap">
                            <div class="card-body">
                                <h6 class="card-title mb-0">LG 27UK850-W 27" 4K UHD IPS Monitor with HDR10</h6>
                                <p class="card-text h6">$408.77</p>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex flex-wrap flex-row">
                                    <div class="w-50">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                    <div class="w-50 text-right">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </a>
                        <div class="overlay">
                            <div class="detail-btn">
                                <a href="#" class="btn btn-sm btn-light font-weight-bold">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mobile mt-5">
    <div class="container">
        <div class="d-flex flex-row flex-wrap">
            <div class="new-arrival-header mx-auto">
                <hr class="m-0 w-75 mx-auto header_hr">
                <h3 class="font-weight-bold text-edited">Mobiles & Tablets</h3>
            </div>
        </div>
        <div class="d-flex flex-wrap flex-row mb-1">
            <small class="text-edited ml-auto">
                more
                <i class="fas fa-angle-double-right"></i>
            </small>
        </div>
        <!-- Swiper -->
        <div class="swiper-container swiperItem">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="card card-product border h-100">
                        <a href="#">
                            <img class="card-img-top " style="height: 150px;" src="{{asset('images/items/mobiles/Untitled-1.jpg')}}" alt="Card image cap">
                            <div class="card-body">
                                <h6 class="card-title mb-0">Apple iPhone 11 -128GB Dual Sim Card</h6>
                                <p class="card-text h6">$911.74</p>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex flex-wrap flex-row">
                                    <div class="w-50">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                    <div class="w-50 text-right">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </a>
                        <div class="overlay">
                            <div class="detail-btn">
                                <a href="#" class="btn btn-sm btn-light font-weight-bold">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="card card-product border h-100">
                        <a href="#">
                            <img class="card-img-top " style="height: 150px;" src="{{asset('images/items/mobiles/Untitled-2.jpg')}}" alt="Card image cap">
                            <div class="card-body">
                                <h6 class="card-title mb-0">iPhone 11 Pro Max Dual SIM With FaceTime</h6>
                                <p class="card-text h6">$1492.98</p>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex flex-wrap flex-row">
                                    <div class="w-50">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                    <div class="w-50 text-right">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </a>
                        <div class="overlay">
                            <div class="detail-btn">
                                <a href="#" class="btn btn-sm btn-light font-weight-bold">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="card card-product border h-100">
                        <a href="#">
                            <img class="card-img-top " style="height: 150px;" src="{{asset('images/items/mobiles/Untitled-3.jpg')}}" alt="Card image cap">
                            <div class="card-body">
                                <h6 class="card-title mb-0">Xiaomi Mi Mix 128GB UFS2.0 ROM 209 g</h6>
                                <p class="card-text h6">$489.80</p>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex flex-wrap flex-row">
                                    <div class="w-50">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                    <div class="w-50 text-right">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </a>
                        <div class="overlay">
                            <div class="detail-btn">
                                <a href="#" class="btn btn-sm btn-light font-weight-bold">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="card card-product border h-100">
                        <a href="#">
                            <img class="card-img-top " style="height: 150px;" src="{{asset('images/items/mobiles/Untitled-4.jpg')}}" alt="Card image cap">
                            <div class="card-body">
                                <h6 class="card-title mb-0">HUAWEI Mate 30 Pro 6.53 inch Wireless</h6>
                                <p class="card-text h6">$868</p>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex flex-wrap flex-row">
                                    <div class="w-50">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                    <div class="w-50 text-right">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </a>
                        <div class="overlay">
                            <div class="detail-btn">
                                <a href="#" class="btn btn-sm btn-light font-weight-bold">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="card card-product border h-100">
                        <a href="#">
                            <img class="card-img-top " style="height: 150px;" src="{{asset('images/items/mobiles/Untitled-7.jpg')}}" alt="Card image cap">
                            <div class="card-body">
                                <h6 class="card-title mb-0">Apple iPad Pro 12.9 In Wi-Fi Cellular 256GB </h6>
                                <p class="card-text h6">$1488.00</p>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex flex-wrap flex-row">
                                    <div class="w-50">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                    <div class="w-50 text-right">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </a>
                        <div class="overlay">
                            <div class="detail-btn">
                                <a href="#" class="btn btn-sm btn-light font-weight-bold">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="more mt-4 mb-5">
    <div class="container">
        <div class="d-flex flex-row flex-wrap">
            <div class="more-btn mx-auto">
                <button type="submit" class="btn btn-sm btn-edited">Load More</button>
            </div>
        </div>
    </div>
</section>