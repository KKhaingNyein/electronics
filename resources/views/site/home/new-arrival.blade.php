<section class="new-arrival mt-5">
    <div class="container">
        <div class="d-flex flex-row flex-wrap">
            <div class="new-arrival-header mx-auto">
                <hr class="m-0 w-75 mx-auto header_hr">
                <h3 class="font-weight-bold text-edited">New Arrival</h3>
            </div>
        </div>
        <div class="d-flex flex-wrap flex-row mb-1">
            <small class="text-edited ml-auto">
                <a href="#" class="">
                    more
                    <i class="fas fa-angle-double-right"></i>
                </a>
            </small>
        </div>
        <!-- Swiper -->
        <div class="swiper-container swiperItem">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="card card-product border h-100">
                        <a href="#">
                            <img class="card-img-top " style="height: 150px;" src="{{asset('images/items/apple.jpg')}}" alt="Card image cap">
                            <div class="card-body">
                                <h6 class="card-title mb-0">iMac Pro (4TB SSD,256GB RAM)</h6>
                                <p class="card-text h6">$4,999</p>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex flex-wrap flex-row">
                                    <div class="w-50">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                    <div class="w-50 text-right">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </a>
                        <div class="overlay">
                            <div class="detail-btn">
                                <a href="#" class="btn btn-sm btn-light font-weight-bold">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="card card-product border h-100">
                        <a href="#">
                            <img class="card-img-top " style="height: 150px;" src="{{asset('images/items/EOS-M200-hero-award_675x450.png')}}" alt="Card image cap">
                            <div class="card-body">
                                <h6 class="card-title mb-0">EOS M200 EF-M 15-45mm IS STM</h6>
                                <p class="card-text h6">$549.99</p>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex flex-wrap flex-row">
                                    <div class="w-50">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                    <div class="w-50 text-right">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </a>
                        <div class="overlay">
                            <div class="detail-btn">
                                <a href="#" class="btn btn-sm btn-light font-weight-bold">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="card card-product border h-100">
                        <a href="#">
                            <img class="card-img-top " style="height: 150px;" src="{{asset('images/items/images.jpg')}}" alt="Card image cap">
                            <div class="card-body">
                                <h6 class="card-title mb-0">Mi LED TV 4A Pro 80cm (32) Black</h6>
                                <p class="card-text h6">$175.46</p>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex flex-wrap flex-row">
                                    <div class="w-50">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                    <div class="w-50 text-right">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </a>
                        <div class="overlay">
                            <div class="detail-btn">
                                <a href="#" class="btn btn-sm btn-light font-weight-bold">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="card card-product border h-100">
                        <a href="#">
                            <img class="card-img-top " style="height: 150px;" src="{{asset('images/items/TWS-BT5-0-Earphones.jpg')}}" alt="Card image cap">
                            <div class="card-body">
                                <h6 class="card-title mb-0">High Quality TWS BT5.0 Earphones</h6>
                                <p class="card-text h6">$27.90</p>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex flex-wrap flex-row">
                                    <div class="w-50">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                    <div class="w-50 text-right">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </a>
                        <div class="overlay">
                            <div class="detail-btn">
                                <a href="#" class="btn btn-sm btn-light font-weight-bold">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="card card-product border h-100">
                        <a href="#">
                            <img class="card-img-top " style="height: 150px;" src="{{asset('images/items/C_4_device_311_0_foto3Colore.png')}}" alt="Card image cap">
                            <div class="card-body">
                                <h6 class="card-title mb-0">Apple iPhone 11 Pro Max (Black)</h6>
                                <p class="card-text h6">$349.99</p>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex flex-wrap flex-row">
                                    <div class="w-50">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                    </div>
                                    <div class="w-50 text-right">
                                        <a href="#" class="text-edited">
                                            <i class="fas fa-heart"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        </a>
                        <div class="overlay">
                            <div class="detail-btn">
                                <a href="#" class="btn btn-sm btn-light font-weight-bold">Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>