<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>@yield('title')</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	@yield('meta')
	@stack('styles')
</head>
<body class="@yield('classes')" id="home">
	<div id="app">
		@include('components.site.search')
		<header class="header_area" id="header">
			@include('components.site.header')
		</header> 
		<main id="main-content mb-4">
			@yield('content')
		</main>
		<footer class="footer-section">
			@include('components.site.footer')
		</footer>
		
	</div>

	<script src="{{ asset('js/app.js') }}"></script>
	<script src="https://unpkg.com/swiper/js/swiper.min.js"></script>

	@yield('script')

</body>
</html>