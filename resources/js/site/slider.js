$(document).ready(function(){
  let shortcut2 = new Swiper('.swiperItem', {
    slidesPerGroup: 1,
    loopFillGroupWithBlank: true,
    slidesPerView: 5,
    spaceBetween: 30,

    breakpoints: {
      576: {
        slidesPerView: 2,
        spaceBetween: 15,
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 30,
      },
      992: {
        slidesPerView: 4,
        spaceBetween: 30,
      },
    },
    autoplay: {
      delay: 3000,
      disableOnInteraction: false,
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    }
  });
});

// $('.carousel').carousel({
//     interval: false
// });