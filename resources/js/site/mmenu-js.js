$(document).ready(function() {

  document.addEventListener(
    "DOMContentLoaded", () => {
      new Mmenu("#my-menu", {
        "extensions": [
        "position-front",
        "position-top",
        "theme-dark"
        ]
      });
    }
    );
});
