$(document).ready(function () {

    $('.sidebar-toggle').click(function (e) {
        e.preventDefault();

        if ($(window).width() <= 768) {
            $('body').toggleClass('sidebar-open');
        } else {
            $('body').toggleClass('sidebar-collapse');
        }
    });

    $('.tree-collapse').click(function (e) {
        e.preventDefault();
        $(this).parent().toggleClass('open');
        let content = this.nextElementSibling;
        if (content.style.maxHeight) {
            content.style.maxHeight = null;
        } else {
            content.style.maxHeight = content.scrollHeight + 'px';
        }
    });
});
