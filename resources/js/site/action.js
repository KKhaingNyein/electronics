$(document).ready(function() {

  $('#search-btn, #closeBtn').on('click', function (event) {
    event.preventDefault();
    $('body').toggleClass('search-form-on');
  });

});


$('.subdrop').hover(function () {
  $(this).find('.dropdown-menu').stop(true, true).fadeIn(100);
}, function () {
  $(this).find('.dropdown-menu').stop(true, true).fadeOut(100);
});